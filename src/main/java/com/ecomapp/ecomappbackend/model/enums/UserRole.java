package com.ecomapp.ecomappbackend.model.enums;

public enum UserRole {
    CUSTOMER,
    STORE_OWNER,
    PLATFORM_MANAGER
}
