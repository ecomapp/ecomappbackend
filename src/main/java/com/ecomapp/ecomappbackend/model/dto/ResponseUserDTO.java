package com.ecomapp.ecomappbackend.model.dto;

import com.ecomapp.ecomappbackend.model.enums.UserRole;
import lombok.Data;

@Data
public class ResponseUserDTO {
    private String username;
    private String password;
    private UserRole userRole;
    private String email;
}
