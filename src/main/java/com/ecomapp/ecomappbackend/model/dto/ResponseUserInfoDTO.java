package com.ecomapp.ecomappbackend.model.dto;

import com.ecomapp.ecomappbackend.model.enums.UserRole;
import lombok.Data;

import java.util.UUID;

@Data
public class ResponseUserInfoDTO {
    private UUID id;
    private UserRole userRole;
}
