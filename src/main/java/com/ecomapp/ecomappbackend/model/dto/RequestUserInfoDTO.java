package com.ecomapp.ecomappbackend.model.dto;

import lombok.Data;
import org.springframework.lang.NonNull;

@Data
public class RequestUserInfoDTO {
    @NonNull
    private String username;
    @NonNull
    private String password;

    private String email;
}
