package com.ecomapp.ecomappbackend.model.entities;

import com.ecomapp.ecomappbackend.model.enums.UserRole;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;


@Entity
@Data
@Table(name = "user_info")
public class UserInfo extends BaseEntity{
    @Column(unique = true)
    private String username;
    private String password;
    private UserRole userRole;
    private String email;
}
