package com.ecomapp.ecomappbackend.model.mappers;

import com.ecomapp.ecomappbackend.model.dto.RequestUserInfoDTO;
import com.ecomapp.ecomappbackend.model.dto.ResponseUserInfoDTO;
import com.ecomapp.ecomappbackend.model.entities.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserInfoMapper {
    UserInfoMapper INSTANCE = Mappers.getMapper(UserInfoMapper.class);

    ResponseUserInfoDTO toResponseDTO(UserInfo userInfo);

    @Mappings({
            @Mapping(source = "requestUserInfoDTO.username", target = "username"),
            @Mapping(source = "requestUserInfoDTO.password", target = "password"),
            @Mapping(source = "requestUserInfoDTO.email", target = "email"),
    })
    UserInfo toEntity(RequestUserInfoDTO requestUserInfoDTO);
}
