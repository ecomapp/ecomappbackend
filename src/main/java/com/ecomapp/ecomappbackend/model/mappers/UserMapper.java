package com.ecomapp.ecomappbackend.model.mappers;

import com.ecomapp.ecomappbackend.model.dto.ResponseUserDTO;
import com.ecomapp.ecomappbackend.model.entities.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    ResponseUserDTO toResponseDTO(UserInfo userInfo);
}
