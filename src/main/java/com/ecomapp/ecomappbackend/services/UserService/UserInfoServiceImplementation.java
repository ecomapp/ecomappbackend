package com.ecomapp.ecomappbackend.services.UserService;

import com.ecomapp.ecomappbackend.model.dto.RequestUserInfoDTO;
import com.ecomapp.ecomappbackend.model.dto.ResponseUserDTO;
import com.ecomapp.ecomappbackend.model.dto.ResponseUserInfoDTO;
import com.ecomapp.ecomappbackend.model.entities.UserInfo;
import com.ecomapp.ecomappbackend.model.enums.UserRole;
import com.ecomapp.ecomappbackend.model.mappers.UserInfoMapper;
import com.ecomapp.ecomappbackend.model.mappers.UserMapper;
import com.ecomapp.ecomappbackend.repositories.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserInfoServiceImplementation implements UserInfoService {
    private final UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoServiceImplementation(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }


    @Override
    public void register(RequestUserInfoDTO requestUserInfoDTO) {
        UserInfo userInfo = UserInfoMapper.INSTANCE.toEntity(requestUserInfoDTO);
        userInfo.setUserRole(UserRole.CUSTOMER);
        userInfoRepository.save(userInfo);
    }

    @Override
    public Optional<ResponseUserInfoDTO> login(RequestUserInfoDTO requestUserInfoDTO) {
        UserInfo loginUserInfo = userInfoRepository.findUserInfoByUsername(requestUserInfoDTO.getUsername());

        if (loginUserInfo != null && loginUserInfo.getPassword().equals(requestUserInfoDTO.getPassword())) {
            return Optional.of(UserInfoMapper.INSTANCE.toResponseDTO(loginUserInfo));
        }

        return Optional.empty();
    }

    @Override
    public ResponseUserDTO getUserByID(UUID id) {
        return UserMapper.INSTANCE.toResponseDTO(userInfoRepository.findUserInfoById(id));
    }
}
