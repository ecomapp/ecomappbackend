package com.ecomapp.ecomappbackend.services.UserService;


import com.ecomapp.ecomappbackend.model.dto.RequestUserInfoDTO;
import com.ecomapp.ecomappbackend.model.dto.ResponseUserDTO;
import com.ecomapp.ecomappbackend.model.dto.ResponseUserInfoDTO;

import java.util.Optional;
import java.util.UUID;

public interface UserInfoService {
    void register(RequestUserInfoDTO requestUserInfoDTO);

    Optional<ResponseUserInfoDTO> login(RequestUserInfoDTO requestUserInfoDTO);

    ResponseUserDTO getUserByID(UUID id);
}
