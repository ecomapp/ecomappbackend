package com.ecomapp.ecomappbackend.repositories;

import com.ecomapp.ecomappbackend.model.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, UUID> {
    UserInfo findUserInfoByUsername(String username);
    UserInfo findUserInfoById(UUID id);
}
